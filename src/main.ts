import 'reflect-metadata';
import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker';
import './plugins/vue-rx';
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';

import 'material-design-icons-iconfont/dist/material-design-icons.css';

import Auth from "@/store/auth";
import {getModule} from "vuex-module-decorators";

Vue.config.productionTip = false;

(async () => {
  try {
    const authModule = getModule(Auth, store);
    if (authModule.user._id !== "" && authModule.token !== "") await authModule.refreshUser();
  } catch (e) {
    console.log("Ah ocurrido un error");
  } finally {
    new Vue({
      router,
      store,
      vuetify,
      render: h => h(App)
    }).$mount('#app')
  }
})();
