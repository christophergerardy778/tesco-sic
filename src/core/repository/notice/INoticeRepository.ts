import {AxiosResponse} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {NoticeEntity} from "@/core/entity/notice/NoticeEntity";
import {GetNoticesPort} from "@/core/repository/notice/port/GetNoticesPort";
import {GetNoticeByIdPort} from "@/core/repository/notice/port/GetNoticeByIdPort";

export interface INoticeRepository {
    getPaginatedNotices(port: GetNoticesPort) : Promise<AxiosResponse<ServerResponse<NoticeEntity[]>>>;
    getNoticeById(port: GetNoticeByIdPort) : Promise<AxiosResponse<ServerResponse<NoticeEntity>>>;
}