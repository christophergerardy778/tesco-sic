import {AxiosResponse} from "axios";

import ClientApi from "@/core/network/ClientApi";

import {NoticeEntity} from "@/core/entity/notice/NoticeEntity";
import {ServerResponse} from "@/core/entity/server/ServerResponse";

import {GetNoticesPort} from "@/core/repository/notice/port/GetNoticesPort";
import {INoticeRepository} from "@/core/repository/notice/INoticeRepository";
import {GetNoticeByIdPort} from "@/core/repository/notice/port/GetNoticeByIdPort";

export default class NoticeRepository implements INoticeRepository {

    getPaginatedNotices(port: GetNoticesPort): Promise<AxiosResponse<ServerResponse<NoticeEntity[]>>> {

        const api = new ClientApi().getAxios();

        return api.get<ServerResponse<NoticeEntity[]>>("/notice", {
            params: {
                page: port.page
            }
        });
    }

    getNoticeById(port: GetNoticeByIdPort): Promise<AxiosResponse<ServerResponse<NoticeEntity>>> {
        const api = new ClientApi().getAxios();
        return api.get<ServerResponse<NoticeEntity>>(`/notice/${port.notice_id}`);
    }
}