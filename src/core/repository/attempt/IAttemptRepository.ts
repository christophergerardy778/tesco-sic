import {AxiosResponse} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {CreateAttemptPort} from "@/core/repository/attempt/port/CreateAttemptPort";
import {GetAttemptsByIdPort} from "@/core/repository/attempt/port/GetAttemptsByIdPort";
import {IAttempt} from "@/core/entity/attempt/AttemptEntity";

export interface IAttemptRepository {
    createAttempt(port: CreateAttemptPort) : Promise<AxiosResponse<ServerResponse<string>>>;
    getAttemptsById(port: GetAttemptsByIdPort) : Promise<AxiosResponse<ServerResponse<IAttempt[]>>>;
}