import {IAttemptRepository} from "@/core/repository/attempt/IAttemptRepository";
import {AxiosResponse} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {CreateAttemptPort} from "@/core/repository/attempt/port/CreateAttemptPort";
import AuthApi from "@/core/network/AuthApi";
import {GetAttemptsByIdPort} from "@/core/repository/attempt/port/GetAttemptsByIdPort";
import {IAttempt} from "@/core/entity/attempt/AttemptEntity";

export default class AttemptRepository implements IAttemptRepository {
    createAttempt(port: CreateAttemptPort): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .post('/attempt', port.data, {
                headers: { "Content-Type": "multipart/form-data" }
            });
    }

    getAttemptsById(port: GetAttemptsByIdPort): Promise<AxiosResponse<ServerResponse<IAttempt[]>>> {
        return new AuthApi()
            .getAxios()
            .get(`/attempt/${port.user_id}`);
    }
}