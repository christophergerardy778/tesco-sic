import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {AxiosResponse} from "axios";
import {UserEntity} from "@/core/entity/auth/UserEntity";
import {UpdateProfilePort} from "@/core/repository/profile/port/UpdateProfilePort";
import {UpdateProfilePhotoPort} from "@/core/repository/profile/port/UpdateProfilePhotoPort";
import {UpdateSkillsPort} from "@/core/repository/profile/port/UpdateSkillsPort";
import {AddExperiencePort} from "@/core/repository/profile/port/AddExperiencePort";
import {DeleteExperiencePort} from "@/core/repository/profile/port/DeleteExperiencePort";

export interface IProfileRepository {
    getProfile(user_id: string) : Promise<AxiosResponse<ServerResponse<UserEntity>>>;
    updateProfile(port: UpdateProfilePort): Promise<AxiosResponse<ServerResponse<string>>>;
    updateProfilePhoto(port: UpdateProfilePhotoPort): Promise<AxiosResponse<ServerResponse<string>>>;
    updateSkills(port: UpdateSkillsPort): Promise<AxiosResponse<ServerResponse<string>>>;
    addExperience(port: AddExperiencePort): Promise<AxiosResponse<ServerResponse<string>>>;
    deleteExperience(port: DeleteExperiencePort): Promise<AxiosResponse<ServerResponse<string>>>;
}