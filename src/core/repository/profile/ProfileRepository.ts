import {IProfileRepository} from "@/core/repository/profile/IProfileRepository";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import AuthApi from "@/core/network/AuthApi";
import {AxiosResponse} from "axios";
import {UserEntity} from "@/core/entity/auth/UserEntity";
import {UpdateProfilePort} from "@/core/repository/profile/port/UpdateProfilePort";
import {UpdateProfilePhotoPort} from "@/core/repository/profile/port/UpdateProfilePhotoPort";
import {UpdateSkillsPort} from "@/core/repository/profile/port/UpdateSkillsPort";
import {AddExperiencePort} from "@/core/repository/profile/port/AddExperiencePort";
import {DeleteExperiencePort} from "@/core/repository/profile/port/DeleteExperiencePort";

export default class ProfileRepository implements IProfileRepository {
    getProfile(user_id: string): Promise<AxiosResponse<ServerResponse<UserEntity>>> {
        return new AuthApi()
            .getAxios()
            .get(`/profile/${user_id}`);
    }

    updateProfile(port: UpdateProfilePort): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .put("/profile/update", port);
    }

    updateProfilePhoto(port: UpdateProfilePhotoPort): Promise<AxiosResponse<ServerResponse<string>>> {
        const formData = new FormData();

        formData.append("id", port.id!);
        formData.append("photo", port.file);

        return new AuthApi()
            .getAxios()
            .put('/profile/photo', formData, {
                headers: {"Content-Type": "multipart/form-data"}
            });
    }

    async updateSkills(port: UpdateSkillsPort): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .put("/profile/skills", port);
    }

    async addExperience(port: AddExperiencePort): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .post("/profile/experience", port);
    }

    async deleteExperience(port: DeleteExperiencePort): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .delete("/profile/experience", {
                data: port
            });
    }
}