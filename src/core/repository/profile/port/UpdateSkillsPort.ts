import {IProfile} from "@/core/entity/profile/IProfile";
import {ISkill} from "@/core/entity/profile/ISkill";

export interface UpdateSkillsPort {
    id: IProfile["_id"];
    skills: ISkill["_id"][];
}