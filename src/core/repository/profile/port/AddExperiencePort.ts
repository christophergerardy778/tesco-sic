import {IProfile} from "@/core/entity/profile/IProfile";
import {IExperience} from "@/core/entity/profile/IExperience";

export interface AddExperiencePort {
    id: IProfile["_id"];
    experience: Partial<IExperience>;
}