import {IProfile} from "@/core/entity/profile/IProfile";

export interface UpdateProfilePhotoPort {
    id: IProfile["_id"];
    file: any;
}