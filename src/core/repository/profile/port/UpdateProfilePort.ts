import {IProfile} from "@/core/entity/profile/IProfile";

export interface UpdateProfilePort {
    id: string;
    profile: Partial<IProfile>;
}