import {IExperience} from "@/core/entity/profile/IExperience";
import {IProfile} from "@/core/entity/profile/IProfile";

export interface DeleteExperiencePort {
    id: IExperience["_id"];
    profile_id: IProfile["_id"];
}