import {AxiosResponse} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IPublication} from "@/core/entity/publications/IPublication";
import {CreatePublicationPort} from "@/core/use_cases/publications/port/CreatePublicationPort";
import {DeletePublicationPort} from "@/core/use_cases/publications/port/DeletePublicationPort";
import {UserEntity} from "@/core/entity/auth/UserEntity";

export interface IPublicationRepository {
    getPaginatedPublications(page: number): Promise<AxiosResponse<ServerResponse<IPublication[]>>>;

    getSinglePublication(publication_id: string): Promise<AxiosResponse<ServerResponse<IPublication>>>;

    getPublicationsByUserId(user_id: UserEntity["_id"]): Promise<AxiosResponse<ServerResponse<IPublication[]>>>;

    createPublication(port: CreatePublicationPort): Promise<AxiosResponse<ServerResponse<string>>>;

    deletePublication(port: DeletePublicationPort): Promise<AxiosResponse<ServerResponse<string>>>
}