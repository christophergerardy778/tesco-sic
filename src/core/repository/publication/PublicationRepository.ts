import {IPublicationRepository} from "@/core/repository/publication/IPublicationRepository";
import {AxiosResponse} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IPublication} from "@/core/entity/publications/IPublication";
import AuthApi from "@/core/network/AuthApi";
import {CreatePublicationPort} from "@/core/use_cases/publications/port/CreatePublicationPort";
import {DeletePublicationPort} from "@/core/use_cases/publications/port/DeletePublicationPort";
import {UserEntity} from "@/core/entity/auth/UserEntity";

export class PublicationRepository implements IPublicationRepository {
    getPaginatedPublications(page: number): Promise<AxiosResponse<ServerResponse<IPublication[]>>> {
        return new AuthApi()
            .getAxios()
            .get("/publication");
    }

    getSinglePublication(publication_id: string): Promise<AxiosResponse<ServerResponse<IPublication>>> {
        return new AuthApi()
            .getAxios()
            .get(`/publication/post/${publication_id}`)
    }

    createPublication(port: CreatePublicationPort): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .post('/publication', {
                user_id: port.user_id,
                publication: port.publication,
                layouts: port.layouts
            });
    }

    deletePublication(port: DeletePublicationPort): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .delete('/publication', {
                data: {
                    publication_id: port.publication_id
                }
            });
    }

    getPublicationsByUserId(user_id: UserEntity["_id"]): Promise<AxiosResponse<ServerResponse<IPublication[]>>> {
        return new AuthApi()
            .getAxios()
            .get(`/publication/user/${user_id}`);
    }

}