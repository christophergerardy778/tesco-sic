import {AxiosResponse} from "axios";
import {UserEntity} from "@/core/entity/auth/UserEntity";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import IAuthRepository from "@/core/repository/auth/IAuthRepository";
import {RegisterPort} from "@/core/repository/auth/port/RegisterPort";
import ClientApi from "@/core/network/ClientApi";
import AuthApi from "@/core/network/AuthApi";

export default class AuthRepository implements IAuthRepository {
    registerByEmail(port: RegisterPort): Promise<AxiosResponse<ServerResponse<UserEntity>>> {
        return new ClientApi()
            .getAxios()
            .post<ServerResponse<UserEntity>>("/auth/register", port);
    }

    loginByEmail(email: string, password: string): Promise<AxiosResponse<ServerResponse<UserEntity>>> {
        return new ClientApi()
            .getAxios()
            .post<ServerResponse<UserEntity>>('/auth/login', {
            email: email,
            password: password
        });
    }

    getUserByToken(token: string): Promise<AxiosResponse<ServerResponse<UserEntity>>> {
        return new AuthApi()
            .getAxios()
            .get(`/auth/${token}`);
    }

    resetPasswordByEmail(email: string): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .post("/auth/reset-password", { email });
    }

    changePassword(password: string, token: string): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .post("/auth/change-password", {
                password,
                token
            });
    }

    updateUser(user_id: string, user: Partial<UserEntity>): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .put("/auth/user", {
                id: user_id,
                user: user
            });
    }
}