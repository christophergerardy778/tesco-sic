import {AxiosResponse} from "axios";
import {UserEntity} from "@/core/entity/auth/UserEntity";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {RegisterPort} from "@/core/repository/auth/port/RegisterPort";

export default interface IAuthRepository {
    registerByEmail(port: RegisterPort) : Promise<AxiosResponse<ServerResponse<UserEntity>>>;
    loginByEmail(email: string, password: string) : Promise<AxiosResponse<ServerResponse<UserEntity>>>;
    getUserByToken(token: string) : Promise<AxiosResponse<ServerResponse<UserEntity>>>;
    resetPasswordByEmail(email: string) : Promise<AxiosResponse<ServerResponse<string>>>;
    changePassword(password: string, token: string) : Promise<AxiosResponse<ServerResponse<string>>>;
    updateUser(user_id: string, user: Partial<UserEntity>): Promise<AxiosResponse<ServerResponse<string>>>;
}