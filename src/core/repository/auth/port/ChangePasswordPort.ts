export interface ChangePasswordPort {
    password: string;
    token: string;
}