import {UserEntity} from "@/core/entity/auth/UserEntity";

export interface UpdateUserPort {
    id: string;
    user: Partial<UserEntity>;
}