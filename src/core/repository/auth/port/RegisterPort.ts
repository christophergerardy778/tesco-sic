export interface RegisterPort {
    name: string,
    lastname: string,
    email: string,
    password: string,
    graduate: boolean,
    dateOfAdmission: string;
}