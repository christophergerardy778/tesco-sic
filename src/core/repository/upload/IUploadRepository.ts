import {AxiosResponse} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IFile} from "@/core/entity/file/IFile";

export interface IUploadRepository {
    uploadFiles(form: FormData): Promise<AxiosResponse<ServerResponse<IFile[]>>>;
    deleteFiles(fileIds: string[]): Promise<AxiosResponse<ServerResponse<string>>>;
}