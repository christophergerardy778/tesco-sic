import {AxiosResponse} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {ISkill} from "@/core/entity/profile/ISkill";

export interface ISkillRepository {
    getAllSkills(): Promise<AxiosResponse<ServerResponse<ISkill[]>>>
}