import {ISkillRepository} from "@/core/repository/skills/ISkillRepository";
import {AxiosResponse} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {ISkill} from "@/core/entity/profile/ISkill";
import AuthApi from "@/core/network/AuthApi";

export class SkillRepository implements ISkillRepository {
    getAllSkills(): Promise<AxiosResponse<ServerResponse<ISkill[]>>> {
        return new AuthApi()
            .getAxios()
            .get("/skill");
    }
}