export enum HTTP {
    SUCCESS = 200,
    NOT_FOUND= 404,
    UNAUTHORIZED = 401
}