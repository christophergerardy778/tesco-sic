import {AxiosResponse} from "axios";
import {HTTP} from "@/core/shared/util/Constants";
import {ServerResponse} from "@/core/entity/server/ServerResponse";

export abstract class HttpCodeHandler<T> {
    handle(response: AxiosResponse<ServerResponse<T>>) : ServerResponse<T> {
        switch (response.status) {

            case HTTP.UNAUTHORIZED:
                return {
                    ok: false,
                    message: response.data.message,
                    error: response.data.error
                };

            case HTTP.NOT_FOUND:
                return {
                    ok: false,
                    error: response.statusText
                };

            case HTTP.SUCCESS:
                if (response.data.ok) {
                    return {
                        ok: true,
                        data: response.data.data,
                        message: response.data.message
                    }
                } else {
                    return {
                        ok: false,
                        error: response.data.error
                    }
                }

            default:
                return {
                    ok: false,
                };
        }
    }
}