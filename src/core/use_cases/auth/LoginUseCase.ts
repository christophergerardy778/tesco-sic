import User, {UserEntity} from "@/core/entity/auth/UserEntity";
import IAuthRepository from "@/core/repository/auth/IAuthRepository";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {LoginPort} from "@/core/repository/auth/port/LoginPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {AxiosError} from "axios";

export default class LoginUseCase
    extends HttpCodeHandler<UserEntity>
    implements BaseUseCase<LoginPort, ServerResponse<UserEntity>>
{

    private authRepository!: IAuthRepository;

    constructor(repository: IAuthRepository) {
        super();
        this.authRepository = repository;
    }

    async execute(port: LoginPort): Promise<ServerResponse<UserEntity>> {
        try {
            const result = await this.authRepository.loginByEmail(port.email, port.password);
            return this.handle(result);
        } catch (e) {
            const errorResponse = e as AxiosError;
            return this.handle(errorResponse.response!);
        }
    }
}