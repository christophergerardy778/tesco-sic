import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {ChangePasswordPort} from "@/core/repository/auth/port/ChangePasswordPort";
import IAuthRepository from "@/core/repository/auth/IAuthRepository";
import AuthRepository from "@/core/repository/auth/AuthRepository";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {AxiosError} from "axios";

class ChangePassword
    extends HttpCodeHandler<string>
    implements BaseUseCase<ChangePasswordPort, ServerResponse<string>>
{
    private readonly repository: IAuthRepository;

    constructor() {
        super();
        this.repository = new AuthRepository();
    }

    async execute(port: ChangePasswordPort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.changePassword(port.password, port.token);
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }
}

export default ChangePassword;