import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {UserEntity} from "@/core/entity/auth/UserEntity";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import IAuthRepository from "@/core/repository/auth/IAuthRepository";
import AuthRepository from "@/core/repository/auth/AuthRepository";
import {AxiosError} from "axios";

class RefreshUserByToken
    extends HttpCodeHandler<UserEntity>
    implements BaseUseCase<string, ServerResponse<UserEntity>>
{

    private repository!: IAuthRepository;

    constructor() {
        super();
        this.repository = new AuthRepository();
    }

    async execute(port: string): Promise<ServerResponse<UserEntity>> {
        try {
            const result = await this.repository.getUserByToken(port);
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }

}

export default RefreshUserByToken;