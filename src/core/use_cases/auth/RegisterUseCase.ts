import {AxiosError} from "axios";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {UserEntity} from "@/core/entity/auth/UserEntity";
import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import IAuthRepository from "@/core/repository/auth/IAuthRepository";
import {RegisterPort} from "@/core/repository/auth/port/RegisterPort";

export default class RegisterUseCase
    extends HttpCodeHandler<UserEntity>
    implements BaseUseCase<RegisterPort, ServerResponse<UserEntity>>
{
    private readonly repository!: IAuthRepository;

    constructor(repository: IAuthRepository) {
        super();
        this.repository = repository;
    }

    async execute(port: RegisterPort): Promise<ServerResponse<UserEntity>> {
        try {
            const result = await this.repository.registerByEmail(port);
            return this.handle(result);
        } catch (e) {
            const errorResponse = e as AxiosError;
            return this.handle(errorResponse.response!);
        }
    }
}