import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import IAuthRepository from "@/core/repository/auth/IAuthRepository";
import AuthRepository from "@/core/repository/auth/AuthRepository";
import {ResetPort} from "@/core/repository/auth/port/ResetPort";
import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {AxiosError} from "axios";

class ResetPassword extends HttpCodeHandler<string> implements BaseUseCase<ResetPort, ServerResponse<string>> {

    private readonly repository!: IAuthRepository;

    constructor() {
        super();
        this.repository = new AuthRepository();
    }

    async execute(port: ResetPort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.resetPasswordByEmail(port.email);
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }
}

export default ResetPassword;