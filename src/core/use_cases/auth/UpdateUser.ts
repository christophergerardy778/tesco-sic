import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {UpdateUserPort} from "@/core/repository/auth/port/UpdateUserPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import IAuthRepository from "@/core/repository/auth/IAuthRepository";
import AuthRepository from "@/core/repository/auth/AuthRepository";
import {AxiosError} from "axios";

export class UpdateUser
    extends HttpCodeHandler<string>
    implements BaseUseCase<UpdateUserPort, ServerResponse<string>> {

    private readonly repository!: IAuthRepository;

    constructor() {
        super();
        this.repository = new AuthRepository();
    }

    async execute(port: UpdateUserPort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.updateUser(port.id, port.user);
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }
}