import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {ISkill} from "@/core/entity/profile/ISkill";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {ISkillRepository} from "@/core/repository/skills/ISkillRepository";
import {SkillRepository} from "@/core/repository/skills/SkillRepository";
import {AxiosError} from "axios";

export class GetAllSkills
    extends HttpCodeHandler<ISkill[]>
    implements BaseUseCase<any, ServerResponse<ISkill[]>> {

    private readonly repository!: ISkillRepository;

    constructor() {
        super();
        this.repository = new SkillRepository();
    }

    async execute(port: any): Promise<ServerResponse<ISkill[]>> {
        try {
            const result = await this.repository.getAllSkills();
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }

}