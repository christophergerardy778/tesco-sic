import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {NoticeEntity} from "@/core/entity/notice/NoticeEntity";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {GetNoticeByIdPort} from "@/core/repository/notice/port/GetNoticeByIdPort";
import {INoticeRepository} from "@/core/repository/notice/INoticeRepository";
import {AxiosError} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";

export default class GetNoticeById
    extends HttpCodeHandler<NoticeEntity>
    implements BaseUseCase<GetNoticeByIdPort, ServerResponse<NoticeEntity>>
{
    private readonly repository!: INoticeRepository;

    constructor(repository: INoticeRepository) {
        super();
        this.repository = repository;
    }

    async execute(port: GetNoticeByIdPort): Promise<ServerResponse<NoticeEntity>> {
        try {
            const response = await this.repository.getNoticeById(port);
            return this.handle(response);
        } catch (e: any) {
            const errorResponse = e as AxiosError;
            return this.handle(errorResponse.response!);
        }
    }
}