import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {INoticeRepository} from "@/core/repository/notice/INoticeRepository";
import {GetNoticesPort} from "@/core/repository/notice/port/GetNoticesPort";
import {NoticeEntity} from "@/core/entity/notice/NoticeEntity";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {AxiosError} from "axios";

export default class GetNotices
    extends HttpCodeHandler<NoticeEntity[]>
    implements BaseUseCase<GetNoticesPort, ServerResponse<NoticeEntity[]>>
{

    private readonly repository!: INoticeRepository;

    constructor(repository: INoticeRepository) {
        super();
        this.repository = repository;
    }

    async execute(port: GetNoticesPort): Promise<ServerResponse<NoticeEntity[]>> {
        try {
            const response = await this.repository.getPaginatedNotices(port);
            return this.handle(response);
        } catch (e: any) {
            const errorResponse = e as AxiosError;
            return this.handle(errorResponse.response!);
        }
    }


}