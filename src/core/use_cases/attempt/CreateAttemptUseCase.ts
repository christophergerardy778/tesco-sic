import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {CreateAttemptPort} from "@/core/repository/attempt/port/CreateAttemptPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IAttemptRepository} from "@/core/repository/attempt/IAttemptRepository";
import AttemptRepository from "@/core/repository/attempt/AttemptRepository";
import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {AxiosError} from "axios";

class CreateAttemptUseCase
    extends HttpCodeHandler<string>
    implements BaseUseCase<CreateAttemptPort, ServerResponse<string>> {

    private readonly repository!: IAttemptRepository;

    constructor() {
        super();
        this.repository = new AttemptRepository();
    }

    async execute(port: CreateAttemptPort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.createAttempt({
                data: port.data
            });
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }
}

export default CreateAttemptUseCase;