import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {GetAttemptsByIdPort} from "@/core/repository/attempt/port/GetAttemptsByIdPort";
import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {IAttemptRepository} from "@/core/repository/attempt/IAttemptRepository";
import AttemptRepository from "@/core/repository/attempt/AttemptRepository";
import {AxiosError} from "axios";
import {IAttempt} from "@/core/entity/attempt/AttemptEntity";

class GetAttemptsByIdUseCase
    extends HttpCodeHandler<IAttempt[]>
    implements BaseUseCase<GetAttemptsByIdPort, ServerResponse<IAttempt[]>> {

    private readonly repository!: IAttemptRepository;

    constructor() {
        super();
        this.repository = new AttemptRepository();
    }


    async execute(port: GetAttemptsByIdPort): Promise<ServerResponse<IAttempt[]>> {
        try {
            const result = await this.repository.getAttemptsById({
                user_id: port.user_id
            });

            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }
}

export default GetAttemptsByIdUseCase;