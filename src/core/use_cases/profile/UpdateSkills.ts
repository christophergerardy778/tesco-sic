import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {UpdateSkillsPort} from "@/core/repository/profile/port/UpdateSkillsPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IProfileRepository} from "@/core/repository/profile/IProfileRepository";
import ProfileRepository from "@/core/repository/profile/ProfileRepository";
import {AxiosError} from "axios";

export class UpdateSkills
    extends HttpCodeHandler<string>
    implements BaseUseCase<UpdateSkillsPort, ServerResponse<string>> {

    private readonly repository!: IProfileRepository;

    constructor() {
        super();
        this.repository = new ProfileRepository();
    }

    async execute(port: UpdateSkillsPort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.updateSkills(port);
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }


}