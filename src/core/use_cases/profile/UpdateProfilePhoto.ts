import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {UpdateProfilePhotoPort} from "@/core/repository/profile/port/UpdateProfilePhotoPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {AxiosError} from "axios";
import {IProfileRepository} from "@/core/repository/profile/IProfileRepository";
import ProfileRepository from "@/core/repository/profile/ProfileRepository";

export class UpdateProfilePhoto
    extends HttpCodeHandler<string>
    implements BaseUseCase<UpdateProfilePhotoPort, ServerResponse<string>> {

    private readonly repository!: IProfileRepository;

    constructor() {
        super();
        this.repository = new ProfileRepository();
    }

    async execute(port: UpdateProfilePhotoPort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.updateProfilePhoto(port);
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }

}