import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {UpdateProfilePort} from "@/core/repository/profile/port/UpdateProfilePort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IProfileRepository} from "@/core/repository/profile/IProfileRepository";
import ProfileRepository from "@/core/repository/profile/ProfileRepository";
import {AxiosError} from "axios";

export class UpdateProfile
    extends HttpCodeHandler<string>
    implements BaseUseCase<UpdateProfilePort, ServerResponse<string>> {

    private readonly repository!: IProfileRepository;

    constructor() {
        super();
        this.repository = new ProfileRepository();
    }

    async execute(port: UpdateProfilePort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.updateProfile(port);
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }

}