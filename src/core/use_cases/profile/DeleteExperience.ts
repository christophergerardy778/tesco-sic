import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {DeleteExperiencePort} from "@/core/repository/profile/port/DeleteExperiencePort";
import {IProfileRepository} from "@/core/repository/profile/IProfileRepository";
import ProfileRepository from "@/core/repository/profile/ProfileRepository";
import {AxiosError} from "axios";

export class DeleteExperience
    extends HttpCodeHandler<string>
    implements BaseUseCase<DeleteExperiencePort, ServerResponse<string>> {

    private readonly repository!: IProfileRepository;

    constructor() {
        super();
        this.repository = new ProfileRepository();
    }

    async execute(port: DeleteExperiencePort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.deleteExperience(port);
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }

}