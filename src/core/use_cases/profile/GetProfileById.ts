import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {GetProfilePort} from "@/core/use_cases/profile/port/GetProfilePort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IProfile} from "@/core/entity/profile/IProfile";
import {IProfileRepository} from "@/core/repository/profile/IProfileRepository";
import {AxiosError} from "axios";
import ProfileRepository from "@/core/repository/profile/ProfileRepository";
import {UserEntity} from "@/core/entity/auth/UserEntity";

class GetProfileById
    extends HttpCodeHandler<UserEntity>
    implements BaseUseCase<GetProfilePort, ServerResponse<UserEntity>> {

    private readonly repository!: IProfileRepository;

    constructor() {
        super();
        this.repository = new ProfileRepository();
    }

    async execute(port: GetProfilePort): Promise<ServerResponse<UserEntity>> {
        try {
            const result = await this.repository.getProfile(port.user_id);
            return this.handle(result);
        } catch (e) {
            const err = e as AxiosError;
            return this.handle(err.response!);
        }
    }

}

export default GetProfileById;