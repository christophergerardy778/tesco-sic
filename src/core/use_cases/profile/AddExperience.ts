import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {AddExperiencePort} from "@/core/repository/profile/port/AddExperiencePort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IProfileRepository} from "@/core/repository/profile/IProfileRepository";
import ProfileRepository from "@/core/repository/profile/ProfileRepository";
import {AxiosError} from "axios";

export class AddExperience
    extends HttpCodeHandler<string>
    implements BaseUseCase<AddExperiencePort, ServerResponse<string>> {

    private readonly repository!: IProfileRepository;

    constructor() {
        super();
        this.repository = new ProfileRepository();
    }

    async execute(port: AddExperiencePort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.addExperience(port);
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }
}