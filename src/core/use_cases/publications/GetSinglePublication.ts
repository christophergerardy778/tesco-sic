import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {IPublication} from "@/core/entity/publications/IPublication";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {GetSinglePublicationPort} from "@/core/use_cases/publications/port/GetSinglePublicationPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IPublicationRepository} from "@/core/repository/publication/IPublicationRepository";
import {PublicationRepository} from "@/core/repository/publication/PublicationRepository";
import {AxiosError} from "axios";

export class GetSinglePublication
    extends HttpCodeHandler<IPublication>
    implements BaseUseCase<GetSinglePublicationPort, ServerResponse<IPublication>> {

    private readonly repository!: IPublicationRepository;

    constructor() {
        super();
        this.repository = new PublicationRepository();
    }

    async execute(port: GetSinglePublicationPort): Promise<ServerResponse<IPublication>> {
        try {
            const result = await this.repository.getSinglePublication(port.publication_id);
            return this.handle(result);
        } catch (e) {
            return this.handle((e as AxiosError).response!);
        }
    }
}