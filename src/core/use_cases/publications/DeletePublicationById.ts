import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {DeletePublicationPort} from "@/core/use_cases/publications/port/DeletePublicationPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {AxiosError} from "axios";
import {IPublicationRepository} from "@/core/repository/publication/IPublicationRepository";
import {PublicationRepository} from "@/core/repository/publication/PublicationRepository";

export class DeletePublicationById
    extends HttpCodeHandler<string>
    implements BaseUseCase<DeletePublicationPort, ServerResponse<string>>{

    private readonly repository!: IPublicationRepository;

    constructor() {
        super();
        this.repository = new PublicationRepository();
    }

    async execute(port: DeletePublicationPort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.deletePublication(port);
            return this.handle(result);
        } catch (e) {
            const err = e as AxiosError;
            return this.handle(err.response!);
        }
    }

}