import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {IPublication} from "@/core/entity/publications/IPublication";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {GetPublicationsByUserIdPort} from "@/core/use_cases/publications/port/GetPublicationsByUserIdPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IPublicationRepository} from "@/core/repository/publication/IPublicationRepository";
import {PublicationRepository} from "@/core/repository/publication/PublicationRepository";
import {AxiosError} from "axios";

export class GetPublicationsByUserId
    extends HttpCodeHandler<IPublication[]>
    implements BaseUseCase<GetPublicationsByUserIdPort, ServerResponse<IPublication[]>> {

    private readonly repository!: IPublicationRepository;

    constructor() {
        super();
        this.repository = new PublicationRepository();
    }

    async execute(port: GetPublicationsByUserIdPort): Promise<ServerResponse<IPublication[]>> {
        try {
            const result = await this.repository.getPublicationsByUserId(port.user_id);
            return this.handle(result);
        } catch (e) {
            return this.handle((e as AxiosError).response!);
        }
    }
}