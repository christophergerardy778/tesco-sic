import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {CreatePublicationPort} from "@/core/use_cases/publications/port/CreatePublicationPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {AxiosError} from "axios";
import {IPublicationRepository} from "@/core/repository/publication/IPublicationRepository";
import {PublicationRepository} from "@/core/repository/publication/PublicationRepository";

export class CreatePublication
    extends HttpCodeHandler<string>
    implements BaseUseCase<CreatePublicationPort, ServerResponse<string>> {

    private readonly repository!: IPublicationRepository;

    constructor() {
        super();
        this.repository = new PublicationRepository();
    }

    async execute(port: CreatePublicationPort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.createPublication(port);
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }
}