import {IPublication} from "@/core/entity/publications/IPublication";
import {ILayout} from "@/core/entity/layouts/ILayout";

export interface CreatePublicationPort {
    user_id: string;
    publication: Partial<IPublication>;
    layouts: ILayout[];
}