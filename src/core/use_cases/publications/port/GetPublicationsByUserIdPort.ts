import {UserEntity} from "@/core/entity/auth/UserEntity";

export interface GetPublicationsByUserIdPort {
    user_id: UserEntity["_id"];
}