export interface GetPaginatedPublicationsPort {
    page: number;
}