import {IPublication} from "@/core/entity/publications/IPublication";

export interface GetSinglePublicationPort {
    publication_id: IPublication["_id"];
}