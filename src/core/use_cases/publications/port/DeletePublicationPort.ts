export interface DeletePublicationPort {
    publication_id: string;
}