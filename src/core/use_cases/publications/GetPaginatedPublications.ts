import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {IPublication} from "@/core/entity/publications/IPublication";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {GetPaginatedPublicationsPort} from "@/core/use_cases/publications/port/GetPaginatedPublicationsPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {AxiosError} from "axios";
import {IPublicationRepository} from "@/core/repository/publication/IPublicationRepository";
import {PublicationRepository} from "@/core/repository/publication/PublicationRepository";

export class GetPaginatedPublications
    extends HttpCodeHandler<IPublication[]>
    implements BaseUseCase<GetPaginatedPublicationsPort, ServerResponse<IPublication[]>> {

    private readonly repository!: IPublicationRepository;

    constructor() {
        super();
        this.repository = new PublicationRepository();
    }

    async execute(port: GetPaginatedPublicationsPort): Promise<ServerResponse<IPublication[]>> {
        try {
            const data = await this.repository.getPaginatedPublications(port.page);
            return this.handle(data);
        } catch (e) {
            const err = e as AxiosError;
            return this.handle(err.response!);
        }
    }
}