import Axios from "axios";

export default Axios.create({
    baseURL: "http://localhost:3000/api",
    //baseURL: "http://192.168.0.2:3000/api",
    //baseURL: "https://tesco-app.loca.lt/api"
});