import {Injectable} from "@scandltd/vue-injector";
import AxiosConfig from "@/core/network/config/AxiosConfig";
import {AxiosInstance} from "axios";

@Injectable
export default class ClientApi {
    private readonly instance: AxiosInstance;

    constructor() {
        this.instance = AxiosConfig;
    }

    public getAxios() : AxiosInstance {
        return this.instance;
    }
}