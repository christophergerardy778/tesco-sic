import { AxiosRequestConfig, AxiosInstance } from "axios";
import AxiosConfig from "@/core/network/config/AxiosConfig";
import store from "@/store";

export default class AuthApi {
    private readonly instance!: AxiosInstance;

    constructor() {
        this.instance = AxiosConfig;
        this.instance.interceptors.request.use(this.createTokenInterceptor);
    }

    private createTokenInterceptor(config: AxiosRequestConfig) : AxiosRequestConfig {
        config.headers.authorization = store.state.Auth.token;
        return config;
    }

    public getAxios() {
        return this.instance;
    }

}