import {UserEntity} from "@/core/entity/auth/UserEntity";
import {ILayout} from "@/core/entity/layouts/ILayout";

export interface IPublication {
    _id: string;
    title: string;
    description: string;
    cover: string | undefined;
    likes: string[];
    user: UserEntity;
    createdAt: string;
    layouts: ILayout[];
}