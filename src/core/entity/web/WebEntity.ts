export interface WebEntityRoute {
    text: string,
    icon: string,
    route: string
}