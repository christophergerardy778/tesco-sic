import {IFile} from "@/core/entity/file/IFile";

export enum LayoutType {
    LINKS = 1,
    FILES = 2
}

export interface ILayout {
    type: number;
    priority: number;

    title: string;
    description: string;
    source: IFile | string;

    links: string[];
    sources: string[] | IFile[];
}