export interface IExperience {
    _id: string;
    company: string;
    job: string;
    start_at: string;
    finish_at: string;
    description: string;
}