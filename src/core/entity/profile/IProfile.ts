import {IExperience} from "@/core/entity/profile/IExperience";
import {ISkill} from "@/core/entity/profile/ISkill";

export interface IProfile {
    _id: string;
    web_url: string;
    facebook: string;
    linkedin: string;
    photo: string;
    description: string;

    has_job: boolean;
    job: string;
    company: string;

    skills: ISkill[];
    experience: IExperience[];
}