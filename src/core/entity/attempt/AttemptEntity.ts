export enum STATUS_ATTEMPT {
    PENDING,
    REJECTED,
    ACCEPTED
}

export interface IAttempt {
    _id: string;
    user: string;
    verificationFile: string;
    status: STATUS_ATTEMPT;
    createdAt: string;
}