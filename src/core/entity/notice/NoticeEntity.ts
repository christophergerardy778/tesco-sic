import {ILayout} from "@/core/entity/layouts/ILayout";

export interface NoticeEntity {
    _id: string;
    title: string;
    description: string;
    cover: string;
    createdAt: string;
    layouts: ILayout[];
}