export enum IFileTypes {
    IMAGE,
    VIDEO,
    PDF,
    DOC
}

export interface IFile {
    _id: string;
    name: string;
    type: IFileTypes;
}