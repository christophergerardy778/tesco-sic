import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import store from '@/store';

import Home from '../views/Home.vue'
import Login from "@/views/auth/Login.vue";
import Register from '@/views/auth/Register.vue'
import AppHome from "@/views/application/AppHome.vue";
import Notices from "@/views/notice/Notices.vue";
import Notice from "@/views/notice/NoticeDetail.vue";
import MainAttempt from "@/views/application/MainAttempt.vue";
import ResetPassword from "@/views/auth/ResetPassword.vue";
import ChangePassword from "@/views/auth/ChangePassword.vue";
import NotFound from "@/views/NotFound.vue";
import Profile from "@/views/application/profile/UserProfile.vue";
import Settings from "@/views/application/settings/Settings.vue";
import PublicationDetail from "@/views/application/publication/PublicationDetail.vue";
import CreatePublicationForm from "@/views/application/publication/CreatePublicationForm.vue";
import Specialist from "@/views/Specialist.vue";
import SpecialistItem from "@/views/SpecialistItem.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      isPublic: true
    }
  },
  {
    path: '/registro',
    name: 'register',
    component: Register,
    meta: {
      authAccess: true
    }
  },
  {
    path: '/ingresar',
    name: 'login',
    component: Login,
    meta: {
      authAccess: true
    }
  },
  {
    path: '/password',
    name: 'forgot-password',
    component: ResetPassword,
    meta: {
      authAccess: true,
    }
  },
  {
    path: '/change-password',
    name: 'change-password',
    component: ChangePassword,
    meta: {
      authAccess: true
    }
  },
  {
    path: '/anuncios',
    name: 'notices',
    component: Notices
  },
  {
    path: '/anuncio/:id',
    name: 'notice',
    component: Notice
  },

  {
    path: '/home',
    name: 'home',
    component: AppHome,

    meta: {
      requireAuth: true,
      requireActiveAccount: true
    }
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile,

    meta: {
      requireAuth: true,
      requireActiveAccount: true
    }
  },
  {
    path: '/configuraciones',
    name: 'settings',
    component: Settings,

    meta: {
      requireAuth: true,
      requireActiveAccount: true
    }
  },
  {
    path: '/activacion',
    name: 'request-attempt',
    component: MainAttempt,
    meta: {
      requireAuth: true,
      requireActiveAccount: false
    }
  },
  {
    path: "/post/:id",
    name: "publication-detail",
    component: PublicationDetail,
    meta: {
      requireAuth: true,
      requireActiveAccount: true
    }
  },
  {
    path: "/crear-post",
    name: "publication-create",
    component: CreatePublicationForm,
    meta: {
      requireAuth: true,
      requireActiveAccount: true
    }
  },
  {
    path: '/especialidades',
    name: 'speciality',
    component: Specialist,
    meta: {
      isPublic: true
    }
  },
  {
    path: '/software',
    name: 'software',
    component: SpecialistItem,
    meta: {
      isPublic: true
    }
  },
  {
    path: '/auditoria',
    name: 'audit',
    component: SpecialistItem,
    meta: {
      isPublic: true
    }
  },
  {
    path: '*',
    name: 'not-found',
    component: NotFound,
    meta: {
      isPublic: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior: () => ({ x: 0, y: 0 })
});

router.beforeEach((to, from, next) => {

  const accountIsActive : boolean = store.state.Auth.user.active;
  const isTokenEmpty: boolean = store.state.Auth.token.length <= 0;
  const isPublic: boolean = to.matched.some(route => route.meta.isPublic);
  const requireAuth: boolean = to.matched.some(route => route.meta.requireAuth);
  const isAuthAccess: boolean = to.matched.some(route => route.meta.authAccess);
  const requireActiveAccount: boolean = to.matched.some(route => route.meta.requireActiveAccount);

  if (isPublic) {
    next()
  } else if (requireAuth) {

    if (!isTokenEmpty) {

      if (requireActiveAccount) {

        if (accountIsActive) next();
        else next({ name: 'request-attempt' });

      } else {

        if (!accountIsActive) next();
        else next({ name: 'home' });

      }

    } else {
      next({ name: 'login' })
    }

  } else if(isAuthAccess) {

    if (isTokenEmpty) next();
    else if (!isTokenEmpty) next({ name: 'home' })

  } else {
    next();
  }
});

export default router
