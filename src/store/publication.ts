import {Action, Module, VuexModule} from "vuex-module-decorators";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {GetPaginatedPublicationsPort} from "@/core/use_cases/publications/port/GetPaginatedPublicationsPort";
import {IPublication} from "@/core/entity/publications/IPublication";
import {GetPaginatedPublications} from "@/core/use_cases/publications/GetPaginatedPublications";
import {GetSinglePublicationPort} from "@/core/use_cases/publications/port/GetSinglePublicationPort";
import {GetSinglePublication} from "@/core/use_cases/publications/GetSinglePublication";
import {CreatePublicationPort} from "@/core/use_cases/publications/port/CreatePublicationPort";
import {DeletePublicationPort} from "@/core/use_cases/publications/port/DeletePublicationPort";
import {CreatePublication} from "@/core/use_cases/publications/CreatePublication";
import {DeletePublicationById} from "@/core/use_cases/publications/DeletePublicationById";
import {GetPublicationsByUserIdPort} from "@/core/use_cases/publications/port/GetPublicationsByUserIdPort";
import {GetPublicationsByUserId} from "@/core/use_cases/publications/GetPublicationsByUserId";

@Module({
    name: "Publication",
    namespaced: true
})
export default class Publication extends VuexModule {
    @Action
    public getPublications(port: GetPaginatedPublicationsPort): Promise<ServerResponse<IPublication[]>> {
        const useCase = new GetPaginatedPublications();
        return useCase.execute(port);
    }

    @Action
    public getSinglePublication(port: GetSinglePublicationPort): Promise<ServerResponse<IPublication>> {
        const useCase = new GetSinglePublication();
        return useCase.execute(port);
    }

    @Action({
        rawError: true
    })
    public async createPublication(port: CreatePublicationPort) : Promise<ServerResponse<string>> {
        const useCase = new CreatePublication();
        return useCase.execute(port);
    }

    @Action
    public async deletePublicationById(port: DeletePublicationPort) : Promise<ServerResponse<string>> {
        const useCase = new DeletePublicationById();
        return useCase.execute(port);
    }

    @Action
    public async getPublicationsByProfileId(port: GetPublicationsByUserIdPort) {
        return new GetPublicationsByUserId().execute(port);
    }
}