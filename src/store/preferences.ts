import {Module, Mutation, VuexModule} from "vuex-module-decorators";

@Module({
    name: "Preferences",
    namespaced: true
})
class Preferences extends VuexModule {

    public darkMode: boolean = false;

    @Mutation
    public switchDarkMode() {
        this.darkMode = !this.darkMode;
    }
}

export default Preferences;