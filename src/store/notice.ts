import {Action, Module, VuexModule} from "vuex-module-decorators";
import GetNotices from "@/core/use_cases/notice/GetNotices";
import NoticeRepository from "@/core/repository/notice/NoticeRepository";
import {GetNoticesPort} from "@/core/repository/notice/port/GetNoticesPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {NoticeEntity} from "@/core/entity/notice/NoticeEntity";
import {GetNoticeByIdPort} from "@/core/repository/notice/port/GetNoticeByIdPort";
import GetNoticeById from "@/core/use_cases/notice/GetNoticeById";

@Module({
    name: "Notice",
    namespaced: true
})
export default class Notice extends VuexModule {
    @Action
    public getNotices(port: GetNoticesPort) :  Promise<ServerResponse<NoticeEntity[]>>{
        const useCase = new GetNotices(new NoticeRepository());
        return useCase.execute(port);
    }

    @Action
    public async getNotice(port: GetNoticeByIdPort) : Promise<ServerResponse<NoticeEntity>>{
        const useCase = new GetNoticeById(new NoticeRepository());
        return useCase.execute(port);
    }
}