import {Action, Module, VuexModule} from "vuex-module-decorators";
import UploadFileUseCase from "@/core/use_cases/upload/UploadFileUseCase";
import DeleteFileUseCase from "@/core/use_cases/upload/DeleteFileUseCase";

@Module({
    name: "Upload",
    namespaced: true
})
class Upload extends VuexModule {
    @Action
    public uploadFile(form: FormData) {
        const useCase = new UploadFileUseCase();
        return useCase.execute({ form });
    }

    @Action
    public deleteFile(files: string[]) {
        const useCase = new DeleteFileUseCase();
        return useCase.execute({ files });
    }
}

export default Upload;