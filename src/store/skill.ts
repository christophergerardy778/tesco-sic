import {Action, Module, VuexModule} from "vuex-module-decorators";
import {GetAllSkills} from "@/core/use_cases/skills/GetAllSkills";

@Module({
    namespaced: true,
    name: 'Skill'
})
class Skill extends VuexModule {

    @Action
    async getAllSkills() {
        const useCase = new GetAllSkills();
        return useCase.execute({});
    }
}

export default Skill;