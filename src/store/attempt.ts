import {Action, Module, VuexModule} from "vuex-module-decorators";
import CreateAttemptUseCase from "@/core/use_cases/attempt/CreateAttemptUseCase";
import {CreateAttemptPort} from "@/core/repository/attempt/port/CreateAttemptPort";
import {GetAttemptsByIdPort} from "@/core/repository/attempt/port/GetAttemptsByIdPort";
import GetAttemptsByIdUseCase from "@/core/use_cases/attempt/GetAttemptsByIdUseCase";

@Module({
    name: 'Attempt',
    namespaced: true
})
export default class Attempt extends VuexModule {
    @Action
    public createAttempt(port: CreateAttemptPort) {
        const useCase = new CreateAttemptUseCase();
        return useCase.execute(port);
    }

    @Action
    public getAttemptsById(port: GetAttemptsByIdPort) {
        const useCase = new GetAttemptsByIdUseCase();
        return useCase.execute(port);
    }
}