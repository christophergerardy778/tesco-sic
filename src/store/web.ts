import {Module, Mutation, VuexModule} from "vuex-module-decorators";
import {WebEntityRoute} from "@/core/entity/web/WebEntity";

@Module({
    namespaced: true,
    name: 'Web'
})
class Web extends VuexModule {

    public showSideMenu = false;

    public menu: WebEntityRoute[] = [
        {
            text: 'Inicio',
            icon: '',
            route: '/'
        },
        {
            text: 'Ingresar',
            icon: '',
            route: '/ingresar'
        },
        {
            text: 'Anuncios',
            icon: '',
            route: '/anuncios'
        },
        {
            text: "Ingenieria de software",
            icon: "",
            route: "/software"
        },
        {
            text: "Seguridad informatica",
            icon: "",
            route: "/auditoria"
        }
    ];

    public menuApp: WebEntityRoute[] = [
        {
            text: 'Inicio',
            icon: 'home',
            route: '/home'
        },
        {
            text: 'Perfil',
            icon: 'person',
            route: '/profile'
        },
        {
            text: 'Configuraciones',
            icon: 'settings',
            route: '/configuraciones'
        },

    ];
}

export default Web;