import {Action, Module, Mutation, VuexModule} from "vuex-module-decorators";
import {UserEntity} from "@/core/entity/auth/UserEntity";
import RegisterUseCase from "@/core/use_cases/auth/RegisterUseCase";
import AuthRepository from "@/core/repository/auth/AuthRepository";
import {RegisterPort} from "@/core/repository/auth/port/RegisterPort";
import {LoginPort} from "@/core/repository/auth/port/LoginPort";
import LoginUseCase from "@/core/use_cases/auth/LoginUseCase";
import RefreshUserByToken from "@/core/use_cases/auth/RefreshUserByToken";
import ResetPassword from "@/core/use_cases/auth/ResetPassword";
import {ResetPort} from "@/core/repository/auth/port/ResetPort";
import ChangePassword from "@/core/use_cases/auth/ChangePassword";
import {ChangePasswordPort} from "@/core/repository/auth/port/ChangePasswordPort";
import {UpdateUserPort} from "@/core/repository/auth/port/UpdateUserPort";
import {UpdateUser} from "@/core/use_cases/auth/UpdateUser";

@Module({
    name: "Auth",
    namespaced: true
})
export default class Auth extends VuexModule {
    public user: UserEntity = {
        _id: "",
        name: "",
        email: "",
        lastname: "",
        password: "",
        enrollment: "",
        active: false,
        graduate: false,
        egressDate: undefined,
        dateOfAdmission: undefined
    };

    public token: string = '';

    @Action
    public async registerByEmail(port: RegisterPort) {
        const useCase = new RegisterUseCase(new AuthRepository());
        return useCase.execute(port);
    }

    @Action
    public async loginByEmail(port: LoginPort) {
        const useCase = new LoginUseCase(new AuthRepository());
        return useCase.execute(port);
    }

    @Action
    public async refreshUser() {
        const useCase = new RefreshUserByToken();
        const result = await useCase.execute(this.token);
        this.setUser(result.data!);
    }

    @Action
    public resetPassword(port: ResetPort) {
        const useCase = new ResetPassword();
        return useCase.execute(port);
    }

    @Action
    public changePassword(port: ChangePasswordPort) {
        const useCase = new ChangePassword();
        return useCase.execute(port);
    }

    @Action
    public logout() {
        this.setToken("");

        this.setUser({
            _id: "",
            name: "",
            email: "",
            lastname: "",
            password: "",
            enrollment: "",
            active: false,
            graduate: false,
            egressDate: undefined,
            dateOfAdmission: undefined
        });
    }

    @Action
    public updateUser(port: UpdateUserPort) {
        const useCase = new UpdateUser();
        return useCase.execute(port);
    }

    @Mutation
    public setUser(user: UserEntity) {
        this.user = user;
    }

    @Mutation
    public setToken(token: string) {
        this.token = token;
    }
}