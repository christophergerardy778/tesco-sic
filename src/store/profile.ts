import {Action, Module, VuexModule} from "vuex-module-decorators";
import GetProfileById from "@/core/use_cases/profile/GetProfileById";
import {UpdateProfilePort} from "@/core/repository/profile/port/UpdateProfilePort";
import {UpdateProfile} from "@/core/use_cases/profile/UpdateProfile";
import {UpdateProfilePhotoPort} from "@/core/repository/profile/port/UpdateProfilePhotoPort";
import {UpdateProfilePhoto} from "@/core/use_cases/profile/UpdateProfilePhoto";
import {UpdateSkills} from "@/core/use_cases/profile/UpdateSkills";
import {UpdateSkillsPort} from "@/core/repository/profile/port/UpdateSkillsPort";
import {AddExperiencePort} from "@/core/repository/profile/port/AddExperiencePort";
import {AddExperience} from "@/core/use_cases/profile/AddExperience";
import {DeleteExperiencePort} from "@/core/repository/profile/port/DeleteExperiencePort";
import {DeleteExperience} from "@/core/use_cases/profile/DeleteExperience";

@Module({
    namespaced: true,
    name: 'Profile',
})
export default class Profile extends VuexModule {

    @Action
    public getProfileById(user_id: string) {
        const useCase = new GetProfileById();
        return useCase.execute({user_id})
    }

    @Action
    public updateProfile(port: UpdateProfilePort) {
        const useCase = new UpdateProfile();
        return useCase.execute(port);
    }

    @Action
    public updateProfilePhoto(port: UpdateProfilePhotoPort) {
        const useCase = new UpdateProfilePhoto();
        return useCase.execute(port);
    }

    @Action
    public updateProfileSkills(port: UpdateSkillsPort) {
        const useCase = new UpdateSkills();
        return useCase.execute(port);
    }

    @Action
    public addExperience(port: AddExperiencePort) {
        const useCase = new AddExperience();
        return useCase.execute(port);
    }

    @Action
    public deleteExperience(port: DeleteExperiencePort) {
        const useCase = new DeleteExperience();
        return useCase.execute(port);
    }
}