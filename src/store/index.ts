import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from "vuex-persist";

import Web from "@/store/web";
import Auth from "@/store/auth";
import Notice from "@/store/notice";
import Preferences from "@/store/preferences";
import Attempt from "@/store/attempt";
import Profile from "@/store/profile";
import Skill from "@/store/skill";
import Publication from "@/store/publication";
import Upload from "@/store/upload";

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

export default new Vuex.Store({
  modules: {
    Web,
    Auth,
    Attempt,
    Notice,
    Preferences,
    Profile,
    Skill,
    Publication,
    Upload
  },
  plugins: [
    vuexLocal.plugin
  ]
})
