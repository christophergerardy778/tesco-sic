importScripts('https://www.gstatic.com/firebasejs/5.5.6/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.5.6/firebase-messaging.js');

const firebaseConfig = {
    apiKey: "AIzaSyDas6dhnMk1jrPBVtna4L1liLLioYTezt8",
    authDomain: "tesco-99fb8.firebaseapp.com",
    projectId: "tesco-99fb8",
    storageBucket: "tesco-99fb8.appspot.com",
    messagingSenderId: "1064011480346",
    appId: "1:1064011480346:web:6e3375572c71c2dd0d40a3",
    measurementId: "G-S3T8CLE4S3"
};

firebase.initializeApp(firebaseConfig);

if (firebase.messaging.isSupported()) {
    console.log("soportado");
    firebase.messaging();
}

const messaging = firebase.messaging();

messaging.getToken({ vapidKey: 'BBhL3CX0evEOzhcwfIpkmo1bKIqmRIG4Igy3EF3JdhMOTQgCjxscSV_OywJN0E-rMClmBXMZehITULoyO4-qZhI' }).then((currentToken) => {
    if (currentToken) {
        console.log(currentToken)
    } else {
        console.log('No registration token available. Request permission to generate one.');
    }
}).catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
});